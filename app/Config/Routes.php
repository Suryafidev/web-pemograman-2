<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('App\Controllers\tokoSembako\HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
// $routes->addPlaceholder('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');


$routes->group('/',  [
	'namespace' => 'App\Controllers\tokoSembako',
], function($routes) {
	$routes->get('/', 'HomeController::index');
	$routes->get('/single', 'HomeController::single'
	//, ['filter' => 'CustomerAuth']
	);
	$routes->get('/login-customer', 'HomeController::login');
});


$routes->group('admin',  [
		'namespace' => 'App\Controllers\admin',
		'filter' => 'role:admin,superadmin'
], function($routes) {

	$routes->get('/', 'DashboardController::index');
	
	$routes->group('customers', ['namespace' => 'App\Controllers\admin\customers'], function($routes) {
		
		$routes->get('/', 'CustomersController::index');
		$routes->get('select', 'CustomersController::select_form');
		$routes->get('(:any)/edit', 'CustomersController::edit/$1');
		$routes->put('(:any)/update', 'CustomersController::update/$1');
		$routes->post('store', 'CustomersController::store');
		$routes->post('(:any)/delete', 'CustomersController::delete/$1');
	});
	
	$routes->group('marketings', [
		'namespace' => 'App\Controllers\admin\marketings',
		'filter' => 'role:superadmin'
	], function($routes) {

		$routes->get('/', 'MarketingsController::index');
		$routes->get('select', 'MarketingsController::select_form');
		$routes->get('(:any)/edit', 'MarketingsController::edit/$1');
		$routes->put('(:any)/update', 'MarketingsController::update/$1');
		$routes->post('store', 'MarketingsController::store');
		$routes->post('(:any)/delete', 'MarketingsController::delete/$1');
	});

	$routes->group('products', ['namespace' => 'App\Controllers\admin\products'], function($routes) {

		$routes->get('/', 'ProductsController::index');
		$routes->get('select', 'ProductsController::select_form');
		$routes->get('(:any)/edit', 'ProductsController::edit/$1');
		$routes->post('(:any)/update', 'ProductsController::update/$1');
		$routes->post('store', 'ProductsController::store');
		$routes->post('(:any)/delete', 'ProductsController::delete/$1');
	});

	$routes->group('category-product', ['namespace' => 'App\Controllers\admin\productCategories'], function($routes) {

		$routes->get('/', 'ProductCategoriesController::index');
		$routes->get('select', 'ProductCategoriesController::select_form');
		$routes->get('(:any)/edit', 'ProductCategoriesController::edit/$1');
		$routes->put('(:any)/update', 'ProductCategoriesController::update/$1');
		$routes->post('store', 'ProductCategoriesController::store');
		$routes->post('(:any)/delete', 'ProductCategoriesController::delete/$1');
	});
	
	$routes->group('brand-product', ['namespace' => 'App\Controllers\admin\productBrands'], function($routes) {

		$routes->get('/', 'ProductBrandsController::index');
		$routes->get('select', 'ProductBrandsController::select_form');
		$routes->get('(:any)/edit', 'ProductBrandsController::edit/$1');
		$routes->post('(:any)/update', 'ProductBrandsController::update/$1');
		$routes->post('store', 'ProductBrandsController::store');
		$routes->post('(:any)/delete', 'ProductBrandsController::delete/$1');
	});
	
	$routes->group('transactions', ['namespace' => 'App\Controllers\admin\transactions'], function($routes) {
		
		$routes->get('/', 'TransactionsController::index');
		$routes->get('select', 'TransactionsController::select_form');
		$routes->get('(:any)/edit', 'TransactionsController::edit/$1');
		$routes->put('(:any)/update', 'TransactionsController::update/$1');
		$routes->post('store', 'TransactionsController::store');
		$routes->post('(:any)/delete', 'TransactionsController::delete/$1');
	});

	$routes->group('banners-products', ['namespace' => 'App\Controllers\admin\banners'], function($routes) {

		$routes->get('/', 'BannersController::index');
		$routes->get('(:any)/edit', 'BannersController::edit/$1');
		$routes->post('(:any)/update', 'BannersController::update/$1');
		$routes->post('store', 'BannersController::store');
		$routes->post('(:any)/delete', 'BannersController::delete/$1');
	});


});


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

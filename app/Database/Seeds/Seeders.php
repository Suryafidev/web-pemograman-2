<?php

namespace App\Database\Seeds;


use App\Models\customers;
use App\Models\products;
use App\Models\marketings;
use App\Models\productCategories;
use App\Models\productBrands;
use App\Models\transactions;
use App\Models\menus;
use App\Models\Banner;
use CodeIgniter\Database\Seeder;
use Myth\Auth\Entities\User;
use Myth\Auth\Authorization\GroupModel;
use Myth\Auth\Authorization\PermissionModel;

class Seeders extends Seeder
{
	public function __construct()
    {
		$this->faker = static::faker();
		$this->marketings = new marketings();
		$this->customers = new customers();
		$this->products = new products();
		$this->productCategories = new productCategories();
		$this->ProductBrands = new productBrands();
		$this->transactions = new transactions();
		$this->menus = new menus();
		$this->banner = new Banner();

		$this->role = new GroupModel();
		$this->permission = new PermissionModel();
    }

	public function run()
	{		
		$this->seed_menus();
		if(!count($this->role->findAll()) > 0){
			$this->seed_roles();
		}
		$this->seed_permission();

		$this->seed_marketings();
		$this->seed_customers();
		$this->seed_products_categories();
		$this->seed_products_brands();
		$this->seed_products();
		$this->seed_Banners();
		$this->seed_transactions();
	}

	
	public function seed_roles()
	{
		$this->role->save([
			"name" => "admin",
			"description" => "role For Admin"
		]);

		$this->role->save([
			"name" => "superadmin",
			"description" => "role For Superadmin"
		]);
	}

	public function seed_menus()
	{
		
		$listMenus = [
			[
			  "name" => "Dashboard",
			  "icon" => "fas fa-tachometer-alt",
			  "link" => "/"
			],
			[
			  "name" => "Customer",
			  "icon" => "fas fa-user",
			  "link" => "/customers"
			],
			[
			  "name" => "Marketing",
			  "icon" => "fas fa-bullhorn",
			  "link" => "/marketings"
			],
			[
			  "name" => "Produk",
			  "icon" => "fas fa-box",
			  "link" => "/products"
			],
			[
			  "name" => "Kategori Produk",
			  "icon" => "fas fa-warehouse",
			  "link" => "/category-product"
			],
			[
			  "name" => "Merek Produk",
			  "icon" => "fas fa-ad",
			  "link" => "/brand-product"
			],
			[
			  "name" => "Banner",
			  "icon" => "fas fa-image",
			  "link" => "/banners-products"
			],
			[
			  "name" => "Transaksi",
			  "icon" => "fas fa-money-check",
			  "link" => "/transactions"  
			]
		];

		foreach ($listMenus as $menu) {
		$this->menus->save([
			"name" => $menu['name'],
			"icon" => $menu['icon'],
			"link" => $menu['link']
		]);
		}
	}


	public function seed_permission()
	{
		$superadmin = $this->role->where("name", "superadmin")->first();
		$admin = $this->role->where("name", "admin")->first();

		$db = \Config\Database::connect();
		
		$this->permission->insert([
			"name" => "admin",
			"description" => "Permission For Admin"
		]);

		
		$db->table('auth_groups_permissions')->insert([
			"permission_id" => $this->permission->InsertId(),
			"group_id" => $superadmin->id
		]);

		$this->permission->save([
			"name" => "superadmin",
			"description" => "Permission For Superadmin"
		]);

		
		$db->table('auth_groups_permissions')->insert([
			"permission_id" => $this->permission->InsertId(),
			"group_id" => $admin->id
		]);



	}


	public function seed_customers()
	{

		for ($i=1; $i <= 14; $i++) { 
			$customerName = $this->faker->name;
			$this->customers->save([
				"code" => "CS".$this->faker->postcode,
				"name" => $customerName,
				"phone_number" => $this->faker->phoneNumber,
				"email" => $this->faker->email,
				"birth_date" => $this->faker->dateTimeBetween($startDate = '-30 year', $endDate = 'now')->format('Y-m-d'),
				"religion" => "Islam",
				"address" => $this->faker->address,
				"postal_code" => $this->faker->postcode,
				"bank_name" => "Mandiri",
				"bank_branch" => "BCA",
				"account_name" => $customerName,
				"account_number" => $this->faker->bankAccountNumber
			]);
		}
	}

	public function seed_marketings()
	{

		$superadmin = $this->role->where("name", "superadmin")->first();
		$admin = $this->role->where("name", "admin")->first();
		
		
		$marketings = [
			[
				'code' => "12180027",
				'name' => 'Brian Surya Jaya',
				'email' => 'naqsaceng.surya@gmail.com',
				'password' => '12180027',
				'gender' => "L",
				'phone_number' => "089517741922",
				"role_id" => $superadmin->id
			],
			[
				'code' => "12180279",
				'name' => 'Theo Chostha Ochktavia',
				'email' => 'theocosta49@yahoo.com',
				'password' => '12180279',
				'gender' => "L",
				'phone_number' => "089653095661",
				"role_id" => $admin->id
			],
			[
				'code' => "12180142",
				'name' => 'Dominggus jabi',
				'email' => 'jabidominggus@gmail.com',
				'password' => '12180142',
				'gender' => "L",
				'phone_number' => "081383466522",
				"role_id" => $admin->id
			],
			[
				'code' => "12180495",
				'name' => 'Muhamad Ilham Maulana',
				'email' => 'ilham.m180999@gmail.com',
				'password' => '12180495',
				'gender' => "L",
				'phone_number' => "085939173893",
				"role_id" => $admin->id
			],
			[
				'code' => "12180603",
				'name' => 'Sustiana Arivin',
				'email' => 'Sustianaarivin@gmail.com',
				'password' => '12180603',
				'gender' => "P",
				'phone_number' => "081285966475",
				"role_id" => $admin->id
			],
			[
				'code' => "12180059",
				'name' => 'Yan Angga Dwipa',
				'email' => 'anggadwifa@gmail.com',
				'password' => '12180059',
				'gender' => "L",
				'phone_number' => "081210907068",
				"role_id" => $admin->id
			],
			[
				'code' => "12180417",
				'name' => 'Anang Julianto',
				'email' => 'anank_julianz@rocketmail.com',
				'password' => '12180417',
				'gender' => "L",
				'phone_number' => "0895392131919",
				"role_id" => $admin->id
			]
		];

		foreach ($marketings as $marketing) {
			$save_marketings = $this->marketings->CreateMarketingUser($marketing);
		}

	}

	public function seed_products_categories()
	{
		$this->productCategories->save([
			"code" => "PC".$this->faker->postcode,
			"name" => "Makanan",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
		]);

		$this->productCategories->save([
			"code" => "PC".$this->faker->postcode,
			"name" => "Minuman",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
		]);

		$this->productCategories->save([
			"code" => "PC".$this->faker->postcode,
			"name" => "Pembersih Lantai",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
		]);

		$this->productCategories->save([
			"code" => "PC".$this->faker->postcode,
			"name" => "Rokok",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
		]);
	}

	public function seed_products_brands()
	{
		$this->ProductBrands->save([
			"code" => "PB".$this->faker->postcode,
			"name" => "Sunlight",
			"description" => 'Sunlight merupakan pembersih ampuh yang membuat peralatan rumah tangga menjadi lebih mudah dibersihkan.\r\n\r\nSunlight sudah tersedia di Indonesia sejak lebih dari 25 tahun yang lalu dengan format batang pada awalnya. Pada tahun 1980an, Sunlight diluncurkan dalam bentuk cair yang menjadikannya sebagai produk cuci piring pertama di Indonesia. Selama 20 tahun, produk Sunlight berhasil menjadi merek cairan cuci piring terbesar di Indonesia dengan berbagai aktivasi inovasi dan promosi. Sunlight sebagai pemimpin pasar, selalu menawarkan solusi terbaik untuk membersihkan peralatan masak dan peralatan dapur dari semua jenis kotoran, bau dan lemak. Sunlight terdiri dari 3 varian yang disukai oleh para pelanggannya yaitu lime, lemon dan strawberry.',
			"image"=> "f41197a5-6ff9-3dd9-b1e5-17bac9f44e48.jpg"
		]);

		$this->ProductBrands->save([
			"code" => "PB".$this->faker->postcode,
			"name" => "Balsem",
			"image"=> "195a8749-c20f-3a6c-b1a2-167e6bd2e210.jpg"
		]);

		$this->ProductBrands->save([
			"code" => "PB".$this->faker->postcode,
			"name" => "Wings",
			"image"=> "2eb4eac7-18c3-36af-8700-98cf5ee54fb5.jpg"
		]);

		$this->ProductBrands->save([
			"code" => "PB".$this->faker->postcode,
			"name" => "Gudang Garam",
			"image"=> "f0492e5d-facc-35d5-aedf-0b80125a02a2.jpg"
		]);
	}

	public function seed_products()
	{		
		$category = $this->productCategories->where("name","Makanan")->first();
		$brand = $this->ProductBrands->where("name","Balsem")->first();

		$this->products->save([
			"category_id" => $category['id'],
			"brand_id" => $brand->id,
			"code" => "P".$this->faker->postcode,
			"name" => "Balsem",
			"price" => "10000",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
			"image" => "514b83f9-7cfa-346f-aa5c-02fe2f7513f1.jpg"
		]);
		
		$category = $this->productCategories->where("name","Minuman")->first();
		$brand = $this->ProductBrands->where("name","Sunlight")->first();

		$this->products->save([
			"category_id" => $category['id'],
			"brand_id" => $brand->id,
			"code" => "P".$this->faker->postcode,
			"name" => "Sunlight",
			"price" => "7000",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
			"image" => "f69e3c98-cc0d-3228-8ce4-c4af86e225e7.jpg"
		]);
		
		$category = $this->productCategories->where("name","Pembersih Lantai")->first();
		$brand = $this->ProductBrands->where("name","Wings")->first();

		$this->products->save([
			"category_id" => $category['id'],
			"brand_id" => $brand->id,
			"code" => "P".$this->faker->postcode,
			"name" => "So Klin Lantai",
			"price" => "3500",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
			"image" => "0e2e1a64-fa1b-33b2-9f9d-afcc1a186337.jpg"
		]);
		
		$category = $this->productCategories->where("name","Rokok")->first();
		$brand = $this->ProductBrands->where("name","Gudang Garam")->first();

		$this->products->save([
			"category_id" => $category['id'],
			"brand_id" => $brand->id,
			"code" => "P".$this->faker->postcode,
			"name" => "Gudang Garam",
			"price" => "15000",
			"description" => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
			"image" => "54049ba0-171c-3d68-83b2-9b942db0be88.jpg"
		]);


	}

	
	public function seed_Banners()
	{
		$this->banner->save([
			"name" => "sunlight showcase",
			"link" => "https://nusamandiri.fiture.id/products/f69e3c98-cc0d-3228-8ce4-c4af86e225e7.jpg",
			"title" => "Minuman Segar",
			"description" => "penyegar tenggokan dan pembersih lambung, nikmat diminum",
			"image" => "2bf17b05-89a7-34f7-8027-2e00622be813.jpg"
		]);
		
		$this->banner->save([
			"name" => "balsem Showcase",
			"link" => "https://nusamandiri.fiture.id/products/514b83f9-7cfa-346f-aa5c-02fe2f7513f1.jpg",
			"title" => "Obat Gosok Untuk anda",
			"description" => "BALSEM merupakan obat gosok yang digunakan untuk membantu meredakan nyeri otot dan sendi seperti nyeri akibat pukulan/memar, keseleo dan nyeri otot punggung. BALSEM juga baik digunakan sebagai balsam pemanasan bagi olahragawan.",
			"image" => "047d539f-b086-35c3-844d-f00c88ad639a.jpg"
		]);
		
		$this->banner->save([
			"name" => "So Klin Lantai showcase",
			"link" => "https://nusamandiri.fiture.id/products/0e2e1a64-fa1b-33b2-9f9d-afcc1a186337.jpg",
			"title" => "pembersih lantai",
			"description" => "Cairan Pembersih Lantai Aroma Sweet Love",
			"image" => "4b05391b-374b-3aa7-8e5a-86245f7b3ee2.jpg"
		]);
	}

	
	public function seed_transactions()
	{
		$status = [
			"approved", "rejected", "pending"
		];

		for ($i=1; $i <= 21; $i++) { 
			$products =  $this->products->orderBy("id", "RANDOM")->first();
			$customers =  $this->customers->orderBy("id", "RANDOM")->first();
			$marketings =  $this->marketings->orderBy("id", "RANDOM")->first();

			$currentStatus = $status[array_rand($status)];

			$this->transactions->save([
				"invoice" => "TRX".$this->faker->postcode,
				"product_id" =>  $products['id'],
				"customer_id" => $customers['id'],
				"qty" => $this->faker->numberBetween($min = 1, $max = 100),
				"status" => $currentStatus,
				"marketing_id" => $currentStatus != "pending" ? $marketings->id : null
			]);
		}
	}

}

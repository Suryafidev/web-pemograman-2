<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addmenus extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id'         => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'name'       => ['type' => 'varchar', 'constraint' => 50], 
            'icon'       => ['type' => 'varchar', 'constraint' => 50],
            'link'       => ['type' => 'varchar', 'constraint' => 50]
        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('menus', true);
	}

	public function down()
	{
		$this->forge->dropTable('menus');
	}
}

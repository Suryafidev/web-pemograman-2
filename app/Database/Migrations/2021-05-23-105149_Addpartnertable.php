<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpartnertable extends Migration
{
	public function up()
	{
		$this->forge->addField([
            'id'                 => ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
			'name'               => ['type' => 'varchar', 'constraint' => 50], 
			'code'               => ['type' => 'VARCHAR', 'constraint' => '100' ],
            'partner_link'       => ['type' => 'varchar', 'constraint' => 50, 'null' => true],
			'description'        => ['type' => 'TEXT', 'null' => true ],
			'image'              => ['type' => 'VARCHAR', 'constraint' => '100', 'null' => true],
			'created_at'         => ['type' => 'datetime', 'null' => true],
            'updated_at'         => ['type' => 'datetime', 'null' => true],
            'deleted_at'         => ['type' => 'datetime', 'null' => true]
        ]);

        $this->forge->addKey('id', true);
        $this->forge->createTable('partners', true);
	}

	public function down()
	{
		$this->forge->dropTable('partners');
	}
}

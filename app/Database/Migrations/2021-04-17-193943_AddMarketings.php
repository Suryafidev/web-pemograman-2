<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddMarketings extends Migration
{
	public function up()
	{
		$this->forge->addField([
			
			'id' => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'code' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'name' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'birth_date' => [
				'type'           => 'DATE',
				'null' => true
			],
			'birth_place' => [
				'type'           => 'VARCHAR',
				'constraint'     => '50',
				'null' => true
			],
			'gender' => [
				'type'           => 'CHAR',
				'constraint'     => '2',
				'null' => true
			],
			'phone_number' => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
				'null' => true
			],
			'created_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'updated_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'deleted_by' => [
				'type'           => 'CHAR',
				'constraint'     => 36,
				'null' => true
			],
			'created_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null' => true
			],
			'deleted_at' => [
				'type'           => 'DATETIME',
				'null' => true
			]
		]);
			
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('marketings');
	}

	public function down()
	{
		$this->forge->dropTable('marketings');
	}
}

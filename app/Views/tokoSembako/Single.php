<?= $this->extend('tokoSembako/layout/app') ?>

<?= $this->section('content') ?>

  
    <!-- Product Single -->
    <section class="single-product">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <figure class="figure">
                    <img src="<?=base_url('assets/tokoSembako/img/sg1.png')?>" class="figure-img img-fluid" >
                    <figcaption class="figure-caption product-thumbnail-container d-flex justify-content-between">
                        <a href="">
                            <img src="<?=base_url('assets/tokoSembako/img/sg2.png')?>">
                        </a>
                        <a href="">
                            <img src="<?=base_url('assets/tokoSembako/img/sg2.png')?>">
                        </a>
                        <a href="">
                            <img src="<?=base_url('assets/tokoSembako/img/sg3.png')?>">
                        </a>
                        <a href="">
                            <img src="<?=base_url('assets/tokoSembako/img/play.png')?>">
                        </a>
                    </figcaption>
                  </figure>
            </div>
            <div class="col-lg-4">
                <h3>
                    Jeans: Giordano XI
                </h3>
                <p class="text-muted">
                    IDR 290.000
                </p>
                <button type="button" class="btn btn-sm" style="background-color:#EAEAEF" >
                    <img src="img/ic_min.png">
                </button>
                <span class="mx-2">
                    20
                </span>
                <button type="button" class="btn btn-sm" style="background-color:#1ABC9C" >
                    <img src="img/ic_plus.png">
                </button>
                <div class="btn-product">
                    <a href="" class="btn btn-warning text-white">
                        Add To Card
                    </a>
                    <a href="" class="btn" style="background-color: #EAEAEF; color: #9b8888 ;">
                        Add To Wishlist
                    </a>
                </div>
                <div class="design-by">
                    <h5>
                        Designed By     
                    </h5>
                    <div class="row">
                        <div class="col-2">
                            <img src="img/dby1.png">
                        </div>
                        <div class="col">
                            <h4>
                                Anne Mortgery
                            </h4>
                            <p>
                                14.2K 
                            <span>
                                Follower
                            </span>
                            </p>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    </section>
    <!-- Akhir Product Single -->

    <!-- Product Description Dan Review -->
    <section class="product-description p-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" 
                          role="tab" aria-controls="description" aria-selected="true">Product Descrpiton</a>
                        </li>
                        
                        <li class="nav-item">
                          <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" 
                          role="tab" aria-controls="review" aria-selected="false">Reviews (20)</a>
                        </li>
                    </ul>
                      <div class="tab-content p-3" id="myTabContent">
                        <div class="tab-pane fade show active product-review" id="description" role="tabpanel" aria-labelledby="description-tab">
                            <p>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore 
                                et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, 
                                no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, 
                                consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore 
                                magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                                Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt
                                    ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo 
                                    dolores et ea rebum. 
                            </p>
                        </div>
                        <div class="tab-pane fade product-review" id="review" role="tabpanel" aria-labelledby="review-tab">
                            <div class="row">
                                <div class="col-1 d-none d-md-block">
                                    <img src="img/an.png">
                                </div>
                            
                                <div class="col d-none d-md-block">
                                    <h5>Anna Yoo</h5>
                                <p>Productnya wadidaw sekali.........</p>
                                </div>
                            </div>
                            
                            <div class="row">
                              <div class="col-1 d-none d-md-block">
                                  <img src="img/an.png">
                              </div>
                          
                              <div class="col d-none d-md-block">
                                  <h5>Anna Yoo</h5>
                              <p>Productnya wadidaw sekali.........</p>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col-1 d-none d-md-block">
                                <img src="img/an.png">
                            </div>
                        
                            <div class="col d-none d-md-block">
                                <h5>Anna Yoo</h5>
                            <p>Productnya wadidaw sekali.........</p>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-1 d-none d-md-block">
                              <img src="img/an.png">
                          </div>
                      
                          <div class="col d-none d-md-block">
                              <h5>Anna Yoo</h5>
                          <p>Productnya wadidaw sekali.........</p>
                          </div>
                      </div>
                      <div class="row">
                        <div class="col-1 d-none d-md-block">
                            <img src="img/an.png">
                        </div>
                    
                        <div class="col d-none d-md-block">
                            <h5>Anna Yoo</h5>
                        <p>Productnya wadidaw sekali.........</p>
                        </div>
                    </div>
                           
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Akhir Product Description Dan Review -->

    <!-- Similiar Product -->
    <section class="similar-product">
      <div class="container">
        <div class="row mb-3">
          <div class="col">
            <h3>Similar Product</h3>
            <p>Pakaian Pelengkap Product Di Atas</p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-4">
            <figure class="figure">
              <img src="img/similar.png" class="figure-img img-fluid">
              <figcaption class="figure-caption">
                <div class="row">
                  <div class="col">
                    <h4>Jeans Pubb</h4>
                    <p>Match 20%</p>
                  </div>
                  <div class="col align-items-center d-flex justify-content-end">
                    <p style="font-size: 18px;">IDR. 290.000</p>
                  </div>
                </div>
              </figcaption>
            </figure>
          </div>

          <div class="col-sm-4">
            <figure class="figure">
              <img src="img/similar.png" class="figure-img img-fluid">
              <figcaption class="figure-caption">
                <div class="row">
                  <div class="col">
                    <h4>Jeans Pubb</h4>
                    <p>Match 20%</p>
                  </div>
                  <div class="col align-items-center d-flex justify-content-end">
                    <p style="font-size: 18px;">IDR. 290.000</p>
                  </div>
                </div>
              </figcaption>
            </figure>
          </div>

          <div class="col-sm-4">
            <figure class="figure">
              <img src="img/similar.png" class="figure-img img-fluid">
              <figcaption class="figure-caption">
                <div class="row">
                  <div class="col">
                    <h4>Jeans Pubb</h4>
                    <p>Match 20%</p>
                  </div>
                  <div class="col align-items-center d-flex justify-content-end">
                    <p style="font-size: 18px;">IDR. 290.000</p>
                  </div>
                </div>
              </figcaption>
            </figure>
          </div>
          
        </div>
      </div>
    </section>
    <!-- Akhir Similar Product  -->




<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>


</script>


<?= $this->endSection() ?>


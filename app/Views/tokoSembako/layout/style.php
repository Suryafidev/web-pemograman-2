  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" 
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" 
    crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('assets/tokoSembako/css/all.css')?>">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700|Montserrat:200,400,600&display=swap"
    rel="stylesheet">

    <!-- My Css -->
    <link rel="stylesheet" href="<?=base_url('assets/tokoSembako/css/style.css')?>">


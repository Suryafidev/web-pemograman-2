<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?= view('tokoSembako/layout/style') ?>

    <?= $this->renderSection('css') ?>
    
    <title>Online Store</title>
  </head>
  <body>

    <?= view('tokoSembako/layout/navbar') ?>
    <?= view('tokoSembako/layout/breadcrumb') ?>
  
    <?= $this->renderSection('content') ?>

    <?= view('tokoSembako/layout/footer') ?>

    <?= view('tokoSembako/layout/scripts') ?>
    <?= $this->renderSection('js') ?>
  </body>
</html>
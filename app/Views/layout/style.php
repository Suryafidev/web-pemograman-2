  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/fontawesome-free/css/all.min.css')?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url('assets/template/dist/css/adminlte.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/toastr/toastr.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')?>">
  <link rel="stylesheet" href="<?=base_url('assets/template/plugins/select2/css/select2.min.css')?>">

  <style>
    .select2-selection__rendered {
      line-height: 32px !important;
    }
    .select2-container .select2-selection--single {
        height: 36px !important;
    }
    .select2-selection__arrow {
        height: 36px !important;
    }
  </style>
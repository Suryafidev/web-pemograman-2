<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Web Pemograman II</title>

    <?= view('layout/style') ?>
    <?= $this->renderSection('css') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">


  <!-- Preloader -->

  <?= view('layout/header') ?>
  
  
  <?= view('layout/sideProfile') ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  <?= view('layout/breadcrumb') ?>

    <!-- Main content -->
    <section class="content">
    
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

        <?= $this->renderSection('content') ?>

        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Tugas Akhir Semester 6 <a href="http://nusamandiri.ac.id">nusamandiri.ac.id</a></strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Versi Aplikasi</b> 1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<?= view('layout/scripts') ?>
<?= $this->renderSection('js') ?>
<script>

  var Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000
  });
              

  function objectifyForm(formArray) {
      //serialize data function
      var returnArray = {};
      for (var i = 0; i < formArray.length; i++){
          returnArray[formArray[i]['name']] = formArray[i]['value'];
      }
      console.log(returnArray);
      return returnArray;
  }
  $(document).find(".overlay").hide()
</script>
</body> 
</html>

<?php
  $current_fullLink = "/" . current_url(true)->getSegment(2);
  $listMenus = (new \App\Models\menus)->all_withFullPath();
?>
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <?php foreach( $listMenus as $menu ) { ?>
      <?php $can_access = has_permission("superadmin") && $menu['link_name'] == "/marketings"; ?>
      <li class="nav-item menu-open">
        <a href="<?= !$can_access ? base_url($menu['link']) : "#"  ?>" class="nav-link  <?= $menu['link_name'] == $current_fullLink ? "active" : "" ?>">
          <i class="nav-icon <?= $menu['icon'] ?>"></i>
          <p> <?= $menu['name'] ?> </p>
          <?php if ($can_access) { ?>
          <span class="right badge"><i class="fas fa-lock"></i></span>
          <?php } ?>
        </a>
      </li>
      <?php } ?>

    </ul>
  </nav>
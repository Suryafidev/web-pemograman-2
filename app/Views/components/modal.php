
    <div class="modal fade" id="<?= $id_modal ?? "modal" ?>">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="overlay-wrapper">
                        <div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">Loading...</div></div>
                        
                <div class="modal-header">
                    <h4 class="modal-title"><?php echo $title ?? current_url(true)->getSegment(1) ?? "Modal"; ?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form>
                    <div class="modal-body">
                        <?= $body ?>

                        <div id='list_error' style='color:#FF0000;'>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <?php if(isset($footer)) {?>

                            <?= $footer ?>

                        <?php } else { ?>
                        
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>

                        <?php } ?>
                    </div>
                </form>

                    </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
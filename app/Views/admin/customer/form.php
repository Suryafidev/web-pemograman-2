
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
            <label>Kode</label>
                <input type="text" class="form-control" placeholder="Kode" name='code' id ='code'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" placeholder="Nama" name='name' id ='name'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nomor HP</label>
                <input type="text" class="form-control" placeholder="Nomor HP"name='phone_number' id ='phone_number'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label >Email</label>
                <input type="email" class="form-control" placeholder="Email" name='email' id ='email'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Alamat</label>
                <textarea class="form-control" rows="3" placeholder="Alamat" name='address' id ='address'></textarea>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Tanggal Lahir:</label>

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask
                        name='birth_date'
                        id ='birth_date'
                    >
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Agama</label>
                <select class="form-control select2" style="width: 100%;" name='religion' id ='religion'>
                <option selected="selected"></option>
                <option value='Islam'>Islam</option>
                <option value='Kristen'>Kristen</option>
                <option value='Protestan'>Protestan</option>
                <option value='Katolik'>Katolik</option>
                <option value='Nasrani'>Nasrani</option>
                <option value='Buddha'>Buddha</option>
                <option value='Hindu'>Hindu</option>
                <option value='Khonghucu'>Khonghucu</option>
                </select>
            </div>
        </div>
    </div>
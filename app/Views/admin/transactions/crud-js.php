<script>

    var formDisable = function(value) {

      ModalTransactions.find("#invoice").attr('disabled', value);
      ModalTransactions.find("#product_id").attr('disabled', value);
      ModalTransactions.find("#customer_id").attr('disabled', value);
      // ModalTransactions.find("#marketing_id").attr('disabled', value);
      ModalTransactions.find("#qty").attr('disabled', value);

    } 

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            formDisable(false);
            ModalTransactions.modal('show');
            ModalTransactions.find(".overlay").show();
            ModalTransactions.find('.modal-title').html('Edit Transaksi');
            ModalTransactions.find('#list_error').html("");
            ModalTransactions.find('form').attr('action', `/admin/transactions/${$(this).data('id')}/update`);
            ModalTransactions.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/transactions/${$(this).data('id')}/edit`);
            Axios.then((response) => {

                

                  
                  ModalTransactions.find("#invoice").val(response.data.invoice);
                  ModalTransactions.find("#product_id").append(
                      `<option selected value="${response.data.product_id}">${response.data.product_name}</option>`
                  ).trigger('change');
                  ModalTransactions.find("#customer_id").append(
                      `<option selected value="${response.data.customer_id}">${response.data.customer_name}</option>`
                  ).trigger('change');


                  // if(response.data.marketing_id){
                  //   ModalTransactions.find("#marketing_id").append(
                  //     `<option selected value="${response.data.marketing_id}">${response.data.marketing_name}</option>`
                  //   ).trigger('change');
                  // } else {
                  //   ModalTransactions.find("#marketing_id").val("").change();
                  // }

                  ModalTransactions.find("#qty").val(response.data.qty);


                  ModalTransactions.find("#status").val(response.data.status).change();


                  if(response.data.status == "rejected"){
                    console.log(response.data.status);
                    formDisable(true);
                  }


                setTimeout(() => ModalTransactions.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalTransactions.find(".overlay").hide()
            });
        });

        
        ModalTransactions.find("#status").on('change', function() {
          if(ModalTransactions.find('form').attr('method') == "PUT"){
            if(this.value != "rejected"){
              formDisable(false);
            } else {
              formDisable(true);
            }
          }
        });


        $(document).on("click","._create",function() {

            ModalTransactions.modal('show');

            ModalTransactions.find(".overlay").show();
            ModalTransactions.find('.modal-title').html('Create Customer');
            ModalTransactions.find('form').attr('action', `/admin/transactions/store`);
            ModalTransactions.find('form').attr('method', `POST`);

            ModalTransactions.find('#list_error').html("");

            ModalTransactions.find("#invoice").val("");
            ModalTransactions.find("#product_id").val("").change();
            ModalTransactions.find("#customer_id").val("").change();
            // ModalTransactions.find("#marketing_id").val("").change();
            ModalTransactions.find("#qty").val("");
            ModalTransactions.find("#status").val("").change();

            formDisable(false);


            setTimeout(() => ModalTransactions.find(".overlay").hide(), 100);
        });
          
        ModalTransactions.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalTransactions.find('#list_error').html("");
            ModalTransactions.find(".overlay").show();
            $form =  $(this);
            const Axios = axios({
              method: $form.attr('method'),
              url: $form.attr('action'),
              data: objectifyForm($form.serializeArray())
            });

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Customer Berhasil Di Update'
                })
                ModalTransactions.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalTransactions.find(".overlay").hide(), 100);
                  ModalTransactions.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalTransactions.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Customer ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/transactions/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Customer Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })
        });
        
        $('#customer_id').select2({
            allowClear: false,
            ajax: {
                url: "<?= base_url('/admin/customers/select') ?>",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                  return {
                    search: params.term
                  };
                },
                processResults: function (data) {
                  return {
                    results: data
                  };
                },
                cache: false,
            },
            placeholder: 'Customer',
        });
        
        $('#product_id').select2({
            allowClear: false,
            ajax: {
                url: "<?= base_url('/admin/products/select') ?>",
                dataType: 'json',
                delay: 0,
                data: function (params) {
                  return {
                    search: params.term
                  };
                },
                processResults: function (data) {
                  return {
                    results: data
                  };
                },
                cache: false,
            },
            placeholder: 'Produk',
        });
        
        // $('#marketing_id').select2({
        //     allowClear: false,
        //     ajax: {
        //         url: "",
        //         dataType: 'json',
        //         delay: 0,
        //         data: function (params) {
        //           return {
        //             search: params.term
        //           };
        //         },
        //         processResults: function (data) {
        //           return {
        //             results: data
        //           };
        //         },
        //         cache: false,
        //     },
        //     placeholder: 'Marketing',
        // });

    });

</script>
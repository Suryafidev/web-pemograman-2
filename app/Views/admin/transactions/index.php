<?= $this->extend('layout/app') ?>

<?= $this->section('content') ?>

  <div class="row">
    <div class="col-12">
      <div class="card">

        <div class="card-header">
          <h3 class="card-title">Transaksi</h3>
        </div>
        <div class="card-body">
        <button type="button" class="btn btn-success _create">Buat Transaksi</button>
          <table id="table-transaction" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Invoice</th>
                <th>Nama Customer</th>
                <th>Nama Produk</th>
                <th>Total Belanja</th>
                <th>Nama Marketing</th>
                <th>Status</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

<?= view('components/modal',[
  "id_modal" => "modal-transactions",
  "title" => "Create Transactions",
  "body" => view('admin/transactions/form')
]) ?>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>

  var ModalTransactions = $("#modal-transactions");

  var transactionsTable = $("#table-transaction").DataTable({
      "ajax" : window.location.href,
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      fnCreatedRow: function (row, data, index) {
          $('td', row).eq(0).html(index + 1);
      },
      "columns": [
          { "data": "id",  "orderable": true, "searchable": false, "width" : "3%"},
          { "data": "invoice"      },
          { "data": "customer"     },
          { "data": "product"      },
          { "data": "total_amount" },
          { "data": "marketing"    },
          { "data": "status"       },
          { "data": "action"       }
        ]	 
  });

  
  function reloadTable() {
      transactionsTable.ajax.url(window.location.href).load();
  }

  $('[data-mask]').inputmask()

</script>

<?= view('admin/transactions/crud-js') ?>

<?= $this->endSection() ?>


<?= $this->extend('layout/app') ?>



<?= $this->section('css') ?>

<style>

  .imagePreview {
      width: 100%;
      height: 180px;
      background-position: center center;
      background:url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
      background-color:#fff;
      background-size: cover;
      background-repeat:no-repeat;
      display: inline-block;
      box-shadow:0px -3px 6px 2px rgba(0,0,0,0.2);
  }

  .btn-primary
  {
    display:block;
    border-radius:0px;
    box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
    margin-top:-5px;
  }
  .imgUp
  {
    margin-bottom:15px;
  }
</style>
<?= $this->endSection() ?>
<?= $this->section('content') ?>

  <div class="row">
    <div class="col-12">
      <div class="card">

        <div class="card-header">
          <h3 class="card-title">Produk</h3>
        </div>
        <div class="card-body">
        <button type="button" class="btn btn-success _create">Buat Produk</button>
          <table id="tabel-products" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Foto</th>
                <th>Merek</th>
                <th>Kode Produk</th>
                <th>Nama Produk</th>
                <th>Kategori</th>
                <th>Harga</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  
<?= view('components/modal',[
  "id_modal" => "modal-products",
  "title" => "Create Products",
  "body" => view('admin/product/form')
]) ?>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>

  var ModalProducts = $("#modal-products");

  var ProductsTable = $("#tabel-products").DataTable({
      "ajax" : window.location.href,
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      fnCreatedRow: function (row, data, index) {
          $('td', row).eq(0).html(index + 1);
      },
      "columns": [
          { "data": "id",  "orderable": true, "searchable": false, "width" : "3%"},
          { "data": "image" },
          { "data": "brand_image" },
          { "data": "code" },
          { "data": "name" },
          { "data": "category" },
          { "data": "price" },
          { "data": "action" },
        ]	 
  });
  
  function reloadTable() {
      ProductsTable.ajax.url(window.location.href).load();
  }

  $('[data-mask]').inputmask()

</script>

<?= view('admin/product/crud-js') ?>

<?= $this->endSection() ?>


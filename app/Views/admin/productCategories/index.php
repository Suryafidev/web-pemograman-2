<?= $this->extend('layout/app') ?>

<?= $this->section('content') ?>

  <div class="row">
    <div class="col-12">
      <div class="card">

        <div class="card-header">
          <h3 class="card-title">Kategori Produk</h3>
        </div>
        <div class="card-body">
        <button type="button" class="btn btn-success _create">Buat Kategori Produk</button>
          <table id="tabel-productCategory" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode Categori</th>
                <th>Nama Produk Kategori</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  
  
<?= view('components/modal',[
  "id_modal" => "modal-productCategory",
  "title" => "Create Product Category",
  "body" => view('admin/productCategories/form')
]) ?>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>

  var ModalProductCategory = $("#modal-productCategory");

  var MarketingTable = $("#tabel-productCategory").DataTable({
        "ajax" : window.location.href,
        "responsive": true, 
        "lengthChange": false, 
        "autoWidth": false,
      fnCreatedRow: function (row, data, index) {
          $('td', row).eq(0).html(index + 1);
      },
      "columns": [
          { "data": "id",  "orderable": true, "searchable": false, "width" : "3%"},
          { "data": "code" },
          { "data": "name" },
          { "data": "action" },
        ]	 
  });
  
  function reloadTable() {
      MarketingTable.ajax.url(window.location.href).load();
  }

  $('[data-mask]').inputmask()

</script>

<?= view('admin/productCategories/crud-js') ?>

<?= $this->endSection() ?>


<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalProductCategory.modal('show');
            ModalProductCategory.find(".overlay").show();
            ModalProductCategory.find('.modal-title').html('Edit Product Category');
            ModalProductCategory.find('#list_error').html("");
            ModalProductCategory.find('form').attr('action', `/admin/category-product/${$(this).data('id')}/update`);
            ModalProductCategory.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/category-product/${$(this).data('id')}/edit`);
            Axios.then((response) => {
             
                ModalProductCategory.find("#code").val(response.data.code);
                ModalProductCategory.find("#name").val(response.data.name);
                ModalProductCategory.find("#description").val(response.data.description);

                setTimeout(() => ModalProductCategory.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalProductCategory.find(".overlay").hide()
            });
        });


        $(document).on("click","._create",function() {

            ModalProductCategory.modal('show');

            ModalProductCategory.find(".overlay").show();
            ModalProductCategory.find('.modal-title').html('Create Product Category');
            ModalProductCategory.find('form').attr('action', `/admin/category-product/store`);
            ModalProductCategory.find('form').attr('method', `POST`);

            ModalProductCategory.find('#list_error').html("");

            ModalProductCategory.find("#code").val("");
            ModalProductCategory.find("#name").val("");
            ModalProductCategory.find("#description").val("");

            setTimeout(() => ModalProductCategory.find(".overlay").hide(), 100);
        });

          
        ModalProductCategory.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalProductCategory.find(".overlay").show();
            ModalProductCategory.find('#list_error').html("");
            $form =  $(this);
            const Axios = axios({
              method: $form.attr('method'),
              url: $form.attr('action'),
              data: objectifyForm($form.serializeArray())
            });

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Kategori Berhasil Di Update'
                })
                ModalProductCategory.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {

                if(error.response.status == 400){
                  setTimeout(() => ModalProductCategory.find(".overlay").hide(), 100);
                  ModalProductCategory.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalProductCategory.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Kategori Produk ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {
                const Axios = axios.post(`/admin/category-product/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                    Swal.fire(
                      'Terhapus!',
                      'Kategori Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });

    });

</script>
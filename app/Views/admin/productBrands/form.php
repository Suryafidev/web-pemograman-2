
    <div class="row d-flex justify-content-center">
        <div class="col-sm-5 imgUp">
        <div class="imagePreview"></div>
            <label class="btn btn-primary"> Upload
                <input type="file" class="uploadFile img" value="Upload Photo" name="brand_image" id="brand_image"  style="width:0%; height:0%; overflow: hidden;">
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
            <label>Kode</label>
                <input type="text" class="form-control" placeholder="Kode" name='code' id ='code'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" placeholder="Nama" name='name' id ='name'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Link Ke Distributor</label>
                <input type="text" class="form-control" placeholder="Link" name='distributor_link' id ='distributor_link'>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" rows="3" placeholder="Deskripsi" name='description' id ='description'></textarea>
            </div>
        </div>
    </div>
    
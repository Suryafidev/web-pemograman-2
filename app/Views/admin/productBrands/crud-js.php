<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalBrands.find("#brand_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()");
            ModalBrands.find("#brand_image").val("");

            ModalBrands.modal('show');
            ModalBrands.find(".overlay").show();
            ModalBrands.find('.modal-title').html('Edit Merek');
            ModalBrands.find('#list_error').html("");
            ModalBrands.find('form').attr('action', `/admin/brand-product/${$(this).data('id')}/update`);
            ModalBrands.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/brand-product/${$(this).data('id')}/edit`);
            Axios.then((response) => {
                
                ModalBrands.find("#code").val(response.data.code);
                ModalBrands.find("#name").val(response.data.name);
                ModalBrands.find("#distributor_link").val(response.data.distributor_link);
                ModalBrands.find("#description").val(response.data.description);


                if(response.data.image){
                  ModalBrands.find("#brand_image").closest(".imgUp").find('.imagePreview').css("background-image", "url(<?= brand_url() ?>/"+response.data.image+")");
                }
                

                setTimeout(() => ModalBrands.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalBrands.find(".overlay").hide()
            }); 
        });


        $(document).on("click","._create",function() {

            ModalBrands.modal('show');

            ModalBrands.find(".overlay").show();
            ModalBrands.find('.modal-title').html('Create Merek');
            ModalBrands.find('form').attr('action', `/admin/brand-product/store`);
            ModalBrands.find('form').attr('method', `POST`);

            ModalBrands.find('#list_error').html("");

            ModalBrands.find("#brand_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()").css("background-position", "center center");
            ModalBrands.find("#brand_image").val("");
            ModalBrands.find("#code").val("");
            ModalBrands.find("#name").val("");
            ModalBrands.find("#distributor_link").val("");
            ModalBrands.find("#description").val("");
            

            setTimeout(() => ModalBrands.find(".overlay").hide(), 100);
        });

          
        ModalBrands.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalBrands.find('#list_error').html("");
            ModalBrands.find(".overlay").show();
            const $form = $(e.target);
            var form_data = new FormData($form[0]);
            const Axios = axios.post($form.attr('action'), form_data);

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Marketing Berhasil Di Update'
                })
                ModalBrands.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalBrands.find(".overlay").hide(), 100);
                  ModalBrands.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalBrands.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Marketing ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/brand-product/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Marketing Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });


      $(document).on("change",".uploadFile", function() {
          var uploadFile = $(this);
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
  
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
  
              reader.onloadend = function(){ // set image data as background of div
                  uploadFile.closest(".imgUp").find('.imagePreview')
                  .css("background-image", "url("+this.result+")")
                  .css("background-position", "center center");
              }
          }
        
      });


    });

</script>


    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
            <label>Kode</label>
                <input type="text" class="form-control" placeholder="Kode" name='code' id ='code'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" placeholder="Nama" name='name' id ='name'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Email</label>
                <input type="email" class="form-control" placeholder="Email" name='email' id ='email'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>password</label>
                <input type="password" class="form-control"  name='password' id ='password'>
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control select2" style="width: 100%;" name='gender' id ='gender'>
                    <option selected value=''>== Pilih Jenis Kelami ==</option>
                    <option value='L'>Laki - Laki</option>
                    <option value='P'>Perempuan</option>
                    <option value='o'>Lainnya</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nomor HP</label>
                <input type="text" class="form-control" placeholder="Nomor HP"name='phone_number' id ='phone_number'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label >Tempat</label>
                <input type="text" class="form-control" placeholder="Tempat Lahir" name='birth_place' id ='birth_place'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Tanggal Lahir:</label>

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd/mm/yyyy" data-mask
                        name='birth_date'
                        id ='birth_date'
                    >
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Role</label>
                <select class="form-control select2" style="width: 100%;" name='role_id' id ='role_id'>
                    <?php 
                        $roles = (new Myth\Auth\Authorization\GroupModel)->findAll();
                        foreach ($roles as $role) {
                    ?>
                    <option value='<?= $role->id ?>'><?= $role->name ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
<?= $this->extend('layout/app') ?>

<?= $this->section('content') ?>

  <div class="row">
    <div class="col-12">
      <div class="card">

        <div class="card-header">
          <h3 class="card-title">Marketing</h3>
        </div>
        <div class="card-body">
        <button type="button" class="btn btn-success _create">Buat Marketing</button>
          <table id="tabel-marketing" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Kode Marketing</th>
                <th>Nama Marketing</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>

  
<?= view('components/modal',[
  "id_modal" => "modal-marketing",
  "title" => "Create Marketing",
  "body" => view('admin/marketing/form')
]) ?>

<?= $this->endSection() ?>

<?= $this->section('js') ?>

<script>

  var ModalMarketing = $("#modal-marketing");

  var MarketingTable = $("#tabel-marketing").DataTable({
      "ajax" : window.location.href,
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      fnCreatedRow: function (row, data, index) {
          $('td', row).eq(0).html(index + 1);
      },
      "columns": [
          { "data": "id",  "orderable": true, "searchable": false, "width" : "3%"},
          { "data": "code" },
          { "data": "name" },
          { "data": "email" },
          { "data": "action" },
        ]	 
  });
  
  function reloadTable() {
      MarketingTable.ajax.url(window.location.href).load();
  }

  $('[data-mask]').inputmask()

</script>

<?= view('admin/marketing/crud-js') ?>

<?= $this->endSection() ?>



    <div class="row d-flex justify-content-center">
        <div class="col-sm-8 imgUp">
        <div class="imagePreview"></div>
            <label class="btn btn-primary"> Upload
                <input type="file" class="uploadFile img" value="Upload Photo" name="banner_image" id="banner_image"  style="width:0%; height:0%; overflow: hidden;">
            </label>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <label>Nama Banner</label>
                <input type="text" class="form-control" placeholder="Nama" name='name' id ='name'>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label>Judul Banner</label>
                <input type="text" class="form-control" placeholder="Nama" name='title' id ='title'>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Link Ke Produk atau lainnya</label>
                <input type="text" class="form-control" placeholder="Link" name='link' id ='link'>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" rows="3" placeholder="Deskripsi" name='description' id ='description'></textarea>
            </div>
        </div>
    </div>
    
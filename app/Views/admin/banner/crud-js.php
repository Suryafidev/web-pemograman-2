<script>

    $(document).ready(function() {

        $(document).on("click","._edit",function() {

            ModalBanners.find("#banner_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()");
            ModalBanners.find("#banner_image").val("");

            ModalBanners.modal('show');
            ModalBanners.find(".overlay").show();
            ModalBanners.find('.modal-title').html('Edit Banner');
            ModalBanners.find('#list_error').html("");
            ModalBanners.find('form').attr('action', `/admin/banners-products/${$(this).data('id')}/update`);
            ModalBanners.find('form').attr('method', `PUT`);

            const Axios = axios.get(`/admin/banners-products/${$(this).data('id')}/edit`);
            Axios.then((response) => {
             
                ModalBanners.find("#name").val(response.data.name);
                ModalBanners.find("#title").val(response.data.title);
                ModalBanners.find("#link").val(response.data.link);
                ModalBanners.find("#description").val(response.data.description);

                if(response.data.image){
                  ModalBanners.find("#banner_image").closest(".imgUp").find('.imagePreview').css("background-image", "url(<?= banner_url() ?>/"+response.data.image+")");
                }
                

                setTimeout(() => ModalBanners.find(".overlay").hide(), 200);
            });
            Axios.catch((error) => {
                console.log(error);
                ModalBanners.find(".overlay").hide()
            }); 
        });


        $(document).on("click","._create",function() {

            ModalBanners.modal('show');

            ModalBanners.find(".overlay").show();
            ModalBanners.find('.modal-title').html('Create Banner');
            ModalBanners.find('form').attr('action', `/admin/banners-products/store`);
            ModalBanners.find('form').attr('method', `POST`);

            ModalBanners.find('#list_error').html("");

            ModalBanners.find("#banner_image").closest(".imgUp").find('.imagePreview').css("background-image", "url()").css("background-position", "center center");
            ModalBanners.find("#banner_image").val("");
            ModalBanners.find("#name").val("");
            ModalBanners.find("#title").val("");
            ModalBanners.find("#link").val("");
            ModalBanners.find("#description").val("");
            

            setTimeout(() => ModalBanners.find(".overlay").hide(), 100);
        });

          
        ModalBanners.find('form').on('submit', function(e) {
            e.preventDefault();
            ModalBanners.find('#list_error').html("");
            ModalBanners.find(".overlay").show();
            const $form = $(e.target);
            var form_data = new FormData($form[0]);
            const Axios = axios.post($form.attr('action'), form_data);

              Axios.then((response) => {              
                Toast.fire({
                  icon: 'success',
                  title: 'Marketing Berhasil Di Update'
                })
                ModalBanners.modal('hide');
                reloadTable();
              });
              Axios.catch((error) => {
                if(error.response.status == 400){
                  setTimeout(() => ModalBanners.find(".overlay").hide(), 100);
                  ModalBanners.find('#list_error').append($.parseHTML(error.response.data));
                  Toast.fire({
                    icon: 'warning',
                    title: 'Harap Cek Kembali Data yang Dimasukkan !'
                  })

                } else {
                  ModalBanners.modal('hide');
                  Toast.fire({
                    icon: 'error',
                    title: 'Terjadi Kendala teknisi'
                  })

                }
              });
        });

        $(document).on("click","._delete",function() {
            Swal.fire({
              title: 'Apakah Anda Yakin?',
              text: "Marketing ini akan di hapus secara Permanen!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then((result) => {
              if (result.isConfirmed) {

                const Axios = axios.post(`/admin/banners-products/${$(this).data('id')}/delete`);
                Axios.then((response) => {

                      
                    Swal.fire(
                      'Terhapus!',
                      'Marketing Ini Terhapus.',
                      'success'
                    )
                    reloadTable();
                    
                });
                Axios.catch((error) => {
                    console.log(error);
                });

              }else{
                  Swal.fire({
                    icon: 'error',
                    text: 'Dibatalkan'
                  })
              }
            })

        });


      $(document).on("change",".uploadFile", function() {
          var uploadFile = $(this);
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
  
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
  
              reader.onloadend = function(){ // set image data as background of div
                  uploadFile.closest(".imgUp").find('.imagePreview')
                  .css("background-image", "url("+this.result+")")
                  .css("background-position", "center center");
              }
          }
        
      });


    });

</script>

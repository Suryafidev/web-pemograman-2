<?php

namespace App\Controllers\tokoSembako;

use App\Models\Banner;
use App\Controllers\BaseController;

class HomeController extends BaseController
{
	
	public function __construct()
    {
		$this->Banner = new Banner();
    }

	public function index()
	{
		$Banners = $this->Banner->select([
			"banner.id",
			"banner.name",
			"banner.link",
			"banner.title",
			"banner.image",
			"banner.description"
		])->findAll();

		return view('tokoSembako/Home', [
			"banners" => $Banners
		]);
	}

	public function single()
	{
		return view('tokoSembako/Single');
	}

	
	public function login()
	{
		return view('tokoSembako/login');
	}

}

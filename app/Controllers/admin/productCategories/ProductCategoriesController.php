<?php

namespace App\Controllers\admin\productCategories;

use App\Models\productCategories;
use CodeIgniter\I18n\Time;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;


class ProductCategoriesController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->productCategories = new productCategories();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			
			$productCategories = $this->productCategories->select([
				"id",
				"code",
				"name"
			])->findAll();

			foreach ($productCategories as $key => $category) {
				$productCategories[$key]['action'] = view('admin/productCategories/datatables/action', $category);
			}
			
			return json_encode([
				'data' => $productCategories
			]);

		}
		
		return view('admin/productCategories/index');
	}

	
	public function select_form()
	{
		if ($this->request->isAJAX()){

			$productCategories = $this->productCategories->select([
				"id",
				"name as text"
			]);
			

			if($this->request->getVar('search')){
				$productCategories = $productCategories
				->like('code', $this->request->getVar('search'))
				->orLike('name', $this->request->getVar('search'));
			}


			$productCategories = $productCategories->findAll();

			return json_encode($productCategories);
		}
	}


	

	public function form_valid_rules()
	{
		return [
			'code' => [
				"label" => "code",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Produk Kategori Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Produk Kategori Wajib Diisi."
				],
			]
		];
	}

	
	public function store()
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}


		$data = $this->request->getVar();
	
		$productCategories = $this->productCategories->save($data);
		
		return $this->respondCreated($productCategories);
	}

	public function edit($id)
	{
		$productCategories = $this->productCategories->select([
			"code",
			"name",
			"description",
		])->find($id);

		return $this->respondCreated($productCategories);
	}
	
	public function update($id)
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}


		$data = $this->request->getVar();
	
		$productCategories = $this->productCategories->update($id, $data);
		
		return $this->respondCreated($productCategories);
	}
	
	public function delete($id)
	{	
		$productCategories = $this->productCategories->delete($id);
		

		return $this->respondCreated($productCategories);
	}
	
}


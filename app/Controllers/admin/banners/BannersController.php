<?php

namespace App\Controllers\admin\banners;

use App\Models\Banner;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Faker\Factory;
use Config\Services;


class BannersController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->Generator = Factory::create();
		$this->Banner = new Banner();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$Banner = $this->Banner->select([
				"banner.id",
				"banner.name",
				"banner.link",
				"banner.title",
				"banner.image",
			])
			->findAll();

			foreach ($Banner as $key => $each) {
				$each->action = view('admin/banner/datatables/action', ['id'=> $each->id]);

				$Bimage =  $each->image;
				if($Bimage){	
					$each->image =  "<img src='". banner_url($Bimage) ."' width='60px'>";
				}
			}
			
			return json_encode([
				'data' => $Banner
			]);

		}
		
		return view('admin/banner/index');
	}

	
	public function form_valid_rules($IncludingImage = true)
	{
		$rules = [
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Banner Wajib Diisi."
				],
			],
			'title' => [
				"label" => "title",
				"rules" => "required",
				"errors" => [
					"required" => "Judul Banner Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Link Banner Wajib Diisi."
				],
			]
		];

		if($IncludingImage){
			$rules['banner_image'] = [
				"label" => "banner_image",
				"rules" => "uploaded[banner_image]",
				"errors" => [
					"uploaded" => "Gambar Banner Wajib dimasukkan."
				],
			];
		}
		return $rules;
	}
	
	public function store()
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}
		

		$data = $this->request->getVar();
		
		if($this->request->getFile('banner_image')){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('banner_image')->store('/banners', $data['image']);
		}


		$products = $this->Banner->save($data);
		
		return $this->respondCreated($products);
	}

	public function edit($id)
	{

		$Banners = $this->Banner->select([
			"banner.name",
			"banner.link",
			"banner.title",
			"banner.image",
			"banner.description",
		])->find($id);

		return $this->respondCreated($Banners);
	}
	
	public function update($id)
	{

		$rules = $this->form_valid_rules(false);

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();
		
		if($this->request->getFile('banner_image')->isValid()){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('banner_image')->store('/banners', $data['image']);
		}

		$products = $this->Banner->update($id, $data);
		
		return $this->respondCreated($products);
	}
	
	public function delete($id)
	{	
		$banners = $this->Banner->delete($id);

		return $this->respondCreated($banners);
	}
	
}


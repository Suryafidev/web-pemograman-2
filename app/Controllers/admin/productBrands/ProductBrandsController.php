<?php

namespace App\Controllers\admin\productBrands;

use App\Models\productBrands;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Faker\Factory;
use Config\Services;


class ProductBrandsController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->Generator = Factory::create();
		$this->ProductBrands = new productBrands();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$ProductBrands = $this->ProductBrands->select([
				"product_brands.id",
				"product_brands.code",
				"product_brands.name",
				"product_brands.distributor_link",
				"product_brands.image",
			])
			->findAll();

			foreach ($ProductBrands as $key => $brand) {
				$brand->action = view('admin/productBrands/datatables/action', ['id'=> $brand->id]);

				$Bimage =  $brand->image;
				if($Bimage){	
					$brand->image =  "<img src='". brand_url($Bimage) ."' width='60px'>";
				}
			}
			
			return json_encode([
				'data' => $ProductBrands
			]);

		}
		
		return view('admin/productBrands/index');
	}

	
	public function select_form()
	{
		if ($this->request->isAJAX()){

			$productBrand = $this->ProductBrands->select([
				"id",
				"name as text"
			]);
			

			if($this->request->getVar('search')){
				$productBrand = $productBrand
				->like('code', $this->request->getVar('search'))
				->orLike('name', $this->request->getVar('search'));
			}


			$productBrand = $productBrand->findAll();

			return json_encode($productBrand);
		}
	}

	
	public function form_valid_rules($IncludingImage = true)
	{
		$rules = [
			'code' => [
				"label" => "code",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Merek Produk Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Merek Produk Wajib Diisi."
				],
			]
		];

		if($IncludingImage){
			$rules['brand_image'] = [
				"label" => "brand_image",
				"rules" => "uploaded[brand_image]",
				"errors" => [
					"uploaded" => "Gambar Merek Produk Wajib dimasukkan."
				],
			];
		}
		return $rules;
	}
	
	public function store()
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}
		

		$data = $this->request->getVar();
		
		if($this->request->getFile('brand_image')){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('brand_image')->store('/brands', $data['image']);
		}


		$products = $this->ProductBrands->save($data);
		
		return $this->respondCreated($products);
	}

	public function edit($id)
	{

		$Banners = $this->ProductBrands->select([
			"product_brands.code",
			"product_brands.name",
			"product_brands.distributor_link",
			"product_brands.image",
			"product_brands.description"
		])->find($id);

		return $this->respondCreated($Banners);
	}
	
	public function update($id)
	{

		$rules = $this->form_valid_rules(false);

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();
		
		if($this->request->getFile('brand_image')->isValid()){
			$data['image'] = $this->Generator->uuid . ".jpg";
			$imagefile = $this->request->getFile('brand_image')->store('/brands', $data['image']);
		}

		$products = $this->ProductBrands->update($id, $data);
		
		return $this->respondCreated($products);
	}
	
	public function delete($id)
	{	
		$banners = $this->ProductBrands->delete($id);

		return $this->respondCreated($banners);
	}
	
}


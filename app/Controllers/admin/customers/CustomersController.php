<?php

namespace App\Controllers\admin\customers;

use App\Models\customers;
use CodeIgniter\I18n\Time;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;


class CustomersController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->customers = new customers();

		helper(['form']);
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$customers = $this->customers->select([
				"id",
				"code",
				"name",
				"email"
			])->findAll();


			foreach ($customers as $key => $customer) {
				$customers[$key]['action'] = view('admin/customer/datatables/action', $customer);
			}
			
			return json_encode([
				'data' => $customers
			]);

		}
		
		return view('admin/customer/index');
	}

	
	public function select_form()
	{
		if ($this->request->isAJAX()){

			$customers = $this->customers->select([
				"id",
				"name as text"
			]);
			

			if($this->request->getVar('search')){
				$customers = $customers
				->like('code', $this->request->getVar('search'))
				->orLike('name', $this->request->getVar('search'));
			}


			$customers = $customers->findAll();

			return json_encode($customers);
		}
	}

	public function form_valid_rules()
	{
		return [
			'code' => [
				"label" => "code",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Customer Wajib Diisi."
				],
			],
			'name' => [
				"label" => "name",
				"rules" => "required",
				"errors" => [
					"required" => "Nama Customer Wajib Diisi."
				],
			],
			'email' => [
				"label" => "email",
				"rules" => "required|valid_email",
				"errors" => [
					"required" => "Email Customer Wajib Diisi.",
					"valid_email" => "Email Tidak Benar.",
				],
			],
			'birth_date' => [
				"label" => "birth_date",
				"rules" => "required|valid_date[d/m/Y]",
				"errors" => [
					"required" => "Tanggal Lahir Wajib Diisi.",
					"valid_date" => "Tanggal Lahir Kurang Lengkap."
				]
			]
		];
	}
	
	public function store()
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();


		$birth_date = Time::createFromFormat('j/m/Y', $this->request->getVar('birth_date'), 'Asia/Jakarta');
		$data->birth_date = $birth_date->toDateString();


	
		$customers = $this->customers->save($data);
		

		return $this->respondCreated($customers);
	}

	
	public function edit($id)
	{
		$customers = $this->customers->select([
			"code",
			"name",
			"phone_number",
			"email",
			"address",
			"DATE_FORMAT(birth_date, '%d/%m/%Y') as birth_date",
			"religion"
		])->find($id);

		return $this->respondCreated($customers);
	}
	
	public function update($id)
	{
		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();

		$birth_date = Time::createFromFormat('j/m/Y', $this->request->getVar('birth_date'), 'Asia/Jakarta');
		$data->birth_date = $birth_date->toDateString();

	
		$customers = $this->customers->update($id, $data);
		

		return $this->respondCreated($customers);
	}
	
	public function delete($id)
	{	
		$customers = new customers();
		$customers = $this->customers->delete($id);
		

		return $this->respondCreated($customers);
	}
	
}


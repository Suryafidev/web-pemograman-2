<?php


namespace App\Controllers\admin\transactions;

use App\Models\transactions;
use App\Models\marketings;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;


class TransactionsController extends BaseController
{
	use ResponseTrait;

	public function __construct()
    {
		$this->transactions = new transactions();
		$this->marketings = new marketings();
    }

	public function index()
	{
		if ($this->request->isAJAX()){

			$transactions = $this->transactions->select([
				"transactions.id",
				"transactions.invoice",
				"products.name as product",
				"customers.name as customer",
				"marketings.name as marketing",
				"sum(transactions.qty * products.price) as total_amount",
				"CASE transactions.status 
					WHEN 'approved' THEN '<div style=\'color:#00FF00;\'> Disetujui </div>'
					WHEN 'rejected' THEN '<div style=\'color:#FF0000;\'> Ditolak </div>'
					ELSE  '<div style=\'color:#DDDD00;\'> Menunggu </div>'
				END as status"
			])
			->join('products', 'products.id = transactions.product_id', "LEFT")
			->join('customers', 'customers.id = transactions.customer_id', "LEFT")
			->join('marketings', 'marketings.id = transactions.marketing_id', "LEFT")
			->GroupBy("transactions.id")
			->OrderBy("transactions.id", "DESC")
			->findAll();

			
			foreach ($transactions as $key => $transaction) {
				$transactions[$key]['action'] = view('admin/transactions/datatables/action', $transaction);
				$transactions[$key]['total_amount'] = rupiah($transactions[$key]['total_amount']);
			}
			
			return json_encode([
				'data' => $transactions
			]);

		}
		
		return view('admin/transactions/index');
	}

	
	
	public function form_valid_rules()
	{
		return [
			'invoice' => [
				"label" => "invoice",
				"rules" => "required",
				"errors" => [
					"required" => "Kode Invoice Wajib Diisi."
				],
			],
			'product_id' => [
				"label" => "product_id",
				"rules" => "required",
				"errors" => [
					"required" => "Silahkan Pilih Produk Terlebih Dahulu.",
				],
			],
			'customer_id' => [
				"label" => "customer_id",
				"rules" => "required",
				"errors" => [
					"required" => "Silahkan Pilih Customer Terlebih Dahulu.",
				],
			],
			'qty' => [
				"label" => "qty",
				"rules" => "required|greater_than[0]",
				"errors" => [
					"required" => "Quantity Produk Wajib Diisi.",
					"greater_than" => "Quantity Harus Lebih dari 0.",
				],
			]
		];
	}
	

	public function store()
	{

		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();

		if(!$this->request->getVar('status')){
			$data->status = "pending";
		} 
		
		$data->marketing_id = user()->marketing_id;

		$transactions = $this->transactions->save($data);
		
		return $this->respondCreated($transactions);
	}

	public function edit($id)
	{
		$transactions = $this->transactions->select([
			"transactions.id",
			"transactions.invoice",			
			"products.id as product_id",
			"products.name as product_name",
			"customers.id as customer_id",
			"customers.name as customer_name",
			"marketings.id as marketing_id",
			"marketings.name as marketing_name",
			"transactions.qty",
			"transactions.status"
		])
		->join('products', 'products.id = transactions.product_id', "LEFT")
		->join('customers', 'customers.id = transactions.customer_id', "LEFT")
		->join('marketings', 'marketings.id = transactions.marketing_id', "LEFT")
		->find($id);

		return $this->respondCreated($transactions);
	}
	
	public function update($id)
	{

		$rules = $this->form_valid_rules();

		if(!$this->validate($rules)){
			return $this->response->setStatusCode(400)->setBody($this->validator->listErrors());
		}

		$data = $this->request->getVar();
		$status_transaction =  $this->transactions->find($id);

		if($status_transaction['status'] != $this->request->getVar('status')){
			$data->marketing_id = user()->marketing_id;
		}
	
		$transactions = $this->transactions->update($id, $data);
		
		return $this->respondCreated($transactions);
	}
	
	public function delete($id)
	{	
		$this->transactions->update($id, [
			"marketing_id" => user()->marketing_id
		]);

		$transactions = $this->transactions->delete($id);

		return $this->respondCreated($transactions);
	}
	
}

